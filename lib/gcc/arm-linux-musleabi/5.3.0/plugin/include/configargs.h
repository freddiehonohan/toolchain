/* Generated automatically. */
static const char configuration_arguments[] = "../src_toolchain/configure --enable-languages=c,c++ --disable-werror --target=arm-linux-musleabi --prefix= --libdir=/lib --disable-multilib --with-sysroot=/arm-linux-musleabi --with-build-sysroot='/obj_sysroot' --enable-tls --disable-libmudflap --disable-libsanitizer --disable-gnu-indirect-function --disable-libmpx --enable-libstdcxx-time";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "tls", "gnu" } };
